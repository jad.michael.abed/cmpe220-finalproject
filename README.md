# CMPE220-FinalProject

## Name
Credit Card Approval Predictor

## Description
The Credit Card Approval Predictor project is an innovative application designed to assess the likelihood of a user being approved for a credit card based on their input data. Utilizing machine learning techniques, the application processes user information entered through a web-based interface and predicts the probability of approval by a credit card company.

## Installation
1. Clone repository:
    - git clone https://gitlab.com/ashley.n.ho/cmpe220-finalproject"
2. Install Flask (Python 3.8 or newer):
    - pip install -m Flask
3. Run website:
    - python credit_website.py
4. Open website at: 
    - http://127.0.0.1:5000/

## Authors
- Faaris Khilji
- Jad Abed
- Akash Kishorbhai Vegada
- Ashley Ho

