import numpy
from sklearn.preprocessing import StandardScaler 
import pickle

import warnings
warnings.filterwarnings("ignore") 


RFC_file = "RFC_ML_Model.pkl"
with open(RFC_file, 'rb') as file:  
    model = pickle.load(file)


scaler_file = "scaler_file.pkl"
with open(scaler_file, 'rb') as file:  
    scaler = pickle.load(file)
    
input_data = [[1,1,0,360000,19305,1905,1,39,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,0]] #data from user will be placed in this array 

input_data_scaled = scaler.transform(input_data)

results = model.predict(input_data_scaled)

print(results)