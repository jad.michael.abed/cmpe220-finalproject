Model Developed in Python in the ML_Training.py file
Portion that loads the ML/Scalar model and processes users input data is in the "Middle_Ware.py" file (temporary name, idk if middleware is a thing)
	- This file needs the front end to provide an array of 29 elements of the users input data, it returns 0 (most likely application is accepted) or 1 (most likely application is 	declined)


Models are based on how much "at risk" the user is a credit user and their probability for being declined a credit card request
	- no definite results were provided in the dataset (for example if they were declined/approved for a card)
	- instead have credit history and frequency/length information on the user's overdue credit
	- according to source https://www.bankrate.com/finance/credit-cards/how-to-recover-from-credit-card-delinquency/ 
		- a bank will forgive delinquency (missed credit payment) under 30 days as long as it is payed off
		- if the user is a delinquent for longer than 30 days, the bank will put a "flag", increased APR rate penalty, and decrease in credit score
		- This flag will last 7 years on the users credit history 
	-Marked anyone with longer than 30 days of overdue credit in the past 7 years as "at risk" and likely to be declined a credit card

Data Preprocessing/Feature Engineering
	- Deleted some features as either it was missing too many elements, redundancy, or is unrelated (may introduce bias)
		-'OCCUPATION_TYPE', 'CODE_GENDER', 'FLAG_MOBIL', 'FLAG_WORK_PHONE', 'FLAG_PHONE', 'FLAG_EMAIL' removed
	- Split the following "object" items into its components, user needs to mark true or false for these items (or a simple drop down)
		- NAME_INCOME_TYPE, NAME_EDUCATION_TYPE, NAME_FAMILY_STATUS, NAME_HOUSING_TYPE (model is expecting only 1 to be true for each item)
		- NAME_INCOME_TYPE split into Commercial associate, Pensioner, State servant 
		- NAME_EDUCATION_TYPE split into Student, Working, Academic degree,Higher education,  Incomplete higher, Lower secondary, Secondary / secondary special 
		- NAME_FAMILY_STATUS split into Civil marriage, Married, Separated, Single / not married, Widow
		- NAME_HOUSING_TYPE Split into Co-op apartment, Home/Condo Owner, Municipal apartment, Office Apartment, Rented apartment, Living with parents 

Data is split and tested with a 80/20 training/testing split

Tested 3 different ML Models for this Project
-linear regression
-K-Nearest Neighbor
-Random Forest Classifer

Linear regression project dropped shortly after some experimentation, very little correlation between the different features)
	- Perphaps some future feature engineering can be done in the future

K-Nearest Neighbor testing with hyperparameter testing
	- Achieved max score of ~0.90

Random Forst Classifier  testing with hyperparameter testing
	- Achieved max score of ~0.9

Need these parameters (in order) from the front-end passed by an array or integrated directly into the front end (29 in total): 

FLAG_OWN_CAR (int), FLAG_OWN_REALTY (int), CNT_CHILDREN (int), AMT_INCOME_TOTAL (int), DAYS_BIRTH (int), DAYS_EMPLOYED (int), CNT_FAM_MEMBERS (BOOL), MONTHS_BALANCE (BOOL), Commercial Associate (BOOL), Pensioner (BOOL), State servant (BOOL), Student (BOOL), Working (BOOL), Academic degree (BOOL), Higher education (BOOL), Incomplete higher (BOOL), Lower secondary (BOOL), Secondary / secondary special (BOOL), Civil marriage (BOOL), Married (BOOL), Separated (BOOL), Single / not married (BOOL), Widow (BOOL), Co-op apartment (BOOL), House / Condo Owner (BOOL), Municipal apartment (BOOL), Office apartment (BOOL), Rented apartment (BOOL), Living with parents (BOOL)