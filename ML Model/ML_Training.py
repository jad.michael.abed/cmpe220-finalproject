import pandas
import numpy
#import matplotlib.pyplot as plt #for debug
#import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler #for scaling data 
from sklearn.neighbors import KNeighborsClassifier #KNN test
from sklearn.ensemble import RandomForestClassifier #RFC test
from sklearn.linear_model import LinearRegression #LR test
from sklearn.model_selection import RandomizedSearchCV #used for hyperparameter optimization 
import pickle #used to export scaler/ML package

import warnings
warnings.filterwarnings("ignore") 

pandas.options.display.max_columns = None #for debugging purposes when printing

credit_application = pandas.read_csv('application_record.csv') #importing data file #1 with application parameters

credit_application.drop(['OCCUPATION_TYPE', 'CODE_GENDER', 'FLAG_MOBIL', 'FLAG_WORK_PHONE', 'FLAG_PHONE', 'FLAG_EMAIL' ], axis=1, inplace=True) #drop irrelevant features, Credit Card Companies rarely base application on these features

#duplicates = credit_application.loc[credit_application.duplicated(subset=['ID'])].head(100) # debug
credit_application[credit_application.DAYS_EMPLOYED > 0] = 0 #value of 0 means not at risk user, value of 1 means at risk user (flagged by Credit Card Company for being delinquent user for 30+ days)

credit_history = pandas.read_csv('credit_record.csv') #import users credit history 

credit_history['STATUS'] = credit_history['STATUS'].replace({'C' : 0, 'X' : 0, '0' : 0, '1' : 1, '2' : 1, '3' : 1, '4' : 1, '5' : 1}) #converting how long user is overdue credit (over 1 month of overdue credit)
credit_history.STATUS = credit_history.STATUS.astype(numpy.int64) #convert to int if not already
credit_history = credit_history.groupby(['ID']).agg({'STATUS': 'sum', 'MONTHS_BALANCE': 'min'}) #since CSV file has muliple entries per user ID, condensing information 
credit_history[credit_history.STATUS > 1] = 1 #value of 0 means not at risk user, value of 1 means at risk user

df = pandas.merge(credit_application, credit_history, on='ID', how='inner') #merge the two files


df['FLAG_OWN_CAR'] = df['FLAG_OWN_CAR'].replace({'Y' : 1, 'N' : 0}) #1 they own a car, 0 they dont own a car
df.FLAG_OWN_CAR = df.FLAG_OWN_CAR.astype(numpy.int64)
df['FLAG_OWN_REALTY'] = df['FLAG_OWN_REALTY'].replace({'Y' : 1, 'N' : 0}) #1 they own a car, 0 they dont own a car
df.FLAG_OWN_REALTY = df.FLAG_OWN_REALTY.astype(numpy.int64)

df = df.join(pandas.get_dummies(df.NAME_INCOME_TYPE)).drop(['NAME_INCOME_TYPE'], axis=1) #expand object data type and will flag each item as True/False
df = df.join(pandas.get_dummies(df.NAME_EDUCATION_TYPE)).drop(['NAME_EDUCATION_TYPE'], axis=1)
df = df.join(pandas.get_dummies(df.NAME_FAMILY_STATUS)).drop(['NAME_FAMILY_STATUS'], axis=1)
df = df.join(pandas.get_dummies(df.NAME_HOUSING_TYPE)).drop(['NAME_HOUSING_TYPE'], axis=1)

#df.info()

#sns.heatmap(df.corr(), annot=True, cmap="YlGnBu")
#plt.show()
#df.hist()
#plt.show()

  
df = df.abs() #convert negative to positive before scaling 

x, y = df.drop(['STATUS', 'ID'], axis=1), df['STATUS'] # User ID should not be used as a parameter in ML Training

training_data_x, test_data_x, training_data_y, test_data_y = train_test_split( x, y, test_size=0.2) #80/20 training/data set split

scaler = StandardScaler() #scaling data
training_data_x_scaled = scaler.fit_transform(training_data_x)
test_data_x_scaled = scaler.fit_transform(test_data_x)

scaler_file = "scaler_file.pkl" #exporting scaling fit file to be reconstructed in other Python file connected to front-end
with open(scaler_file, 'wb') as file:  
    pickle.dump(scaler, file)

# testing KKN and RFC Models
    
KKN_parameter_grid = { #experimenting with hyperparameters to see if I can find high prediction accuracy model 
    "n_neighbors": list(range(1,29)),
    "weights":["uniform", "distance"],
    "algorithm": ["ball_tree", "kd_tree", "brute", "auto" ]
    } 

KNN_model =  KNeighborsClassifier(n_jobs=-1) #training model on all cores   
test = RandomizedSearchCV(KNN_model, KKN_parameter_grid, cv = 5, n_jobs=-1)
test.fit(training_data_x_scaled,training_data_y)

KNN_model = test.best_estimator_ #exporting and using the best values found by the randomized search 
#KNN_model.fit(training_data_x_scaled,training_data_y)
print(KNN_model.score(test_data_x_scaled, test_data_y))

KNN_file = "KNN_ML_Model.pkl" #exporting KNN ML model file
with open(KNN_file, 'wb') as file:  
    pickle.dump(KNN_model, file)
    
RFC_parameter_grid = { #experimenting with hyperparameters to see if I can find high prediction accuracy model 
    'n_estimators': list(range(10,300)),
     'max_depth': [2,4,6,8, None], 
    'max_features': ['sqrt', 'log2', None], 
} 

RFC_model = RandomForestClassifier(n_jobs=-1)
test1 = RandomizedSearchCV(RFC_model, RFC_parameter_grid, cv = 5, n_jobs=-1)
test1.fit(training_data_x_scaled,training_data_y)

RFC_model = test1.best_estimator_  #exporting and using the best values found by the randomized search 
#RFC_model.fit(training_data_x_scaled,training_data_y)
print(RFC_model.score(test_data_x_scaled, test_data_y))

RFC_file = "RFC_ML_Model.pkl" #exporting KNN ML model file
with open(RFC_file, 'wb') as file:  
    pickle.dump(RFC_model, file)
    
